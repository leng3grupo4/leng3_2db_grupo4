
package leng3_2db_grupo4;


public class Cartaz {
    
   
    private String cabecaCartaz;
    
    private String artistaSecundario;
    
    private int dia;
    
    private static String CABECA_CARTAZ_POR_OMISSAO;
    
    private static String ARTISTA_SECUNDARIO_POR_OMISSAO;
    
    private static int DIA_POR_OMISSAO = 0;
    
    
    
    public Cartaz (String cabecacartaz, String artistasecundarios, int dia)  {
        
        this.cabecaCartaz = cabecacartaz;
        this.artistaSecundario =  artistasecundarios;
        this.dia = dia; 

}
    public Cartaz() {
       
       
    }

    
    
    public String getCabecaCartaz() {
        return cabecaCartaz;
    }

    
    public void setCabecaCartaz(String CabecaCartaz) {
        this.cabecaCartaz = CabecaCartaz;
    }

    
    public String getArtistaSecundario() {
        return artistaSecundario;
    }

    
    public void setArtistaSecundario(String ArtistaSecundarios) {
        this.artistaSecundario = ArtistaSecundarios;
    }

    
    public int getDia() {
        return dia;
    }

    
    public void setDia(int dia) {
        this.dia = dia;
    }
   
    public String toString(){
        return String.format("\n \nCabeca de cartaz: "+getCabecaCartaz()+"\nArtistas secundarios: "+getArtistaSecundario()+"\nDia: "+getDia()); 
        
    } 
    
}
