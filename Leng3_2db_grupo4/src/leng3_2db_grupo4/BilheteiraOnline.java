/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

/**
 *
 * @author Asus
 */
public class BilheteiraOnline {
    
    private String enderecoWeb;

    /**
     * @return the Endereco
     */
    public String getEndereco() {
        return enderecoWeb;
    }

    /**
     * @param Endereco the Endereco to set
     */
    public void setEndereco(String Endereco) {
        this.enderecoWeb = Endereco;
    }
    
     public String toString() {
        return String.format("Endereço: " + enderecoWeb);
    }
}
