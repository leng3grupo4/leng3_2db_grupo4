/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Asus
 */
public class ListaBilhetes {

    private List<Bilhete> listaBilhetes;

    public ListaBilhetes() {
        listaBilhetes = new ArrayList<>();
    }

    public boolean adicionarBilhete(Bilhete Bilhete) {
        if (!listaBilhetes.contains(Bilhete)) {
            return listaBilhetes.add(Bilhete);
        }

        return false;
    }

    public boolean guardar(String nomeFicheiro) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(listaBilhetes);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean ler(String nomeFicheiro) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(nomeFicheiro));
            try {
                listaBilhetes = (List<Bilhete>) in.readObject();
            } finally {
                in.close();
            }
            return true;
        } catch (IOException | ClassNotFoundException ex) {
            return false;
        }
    }

    /**
     * @return the listaBilhetes
     */
    public List<Bilhete> getListaBilhetes() {
        return listaBilhetes;
    }

    /**
     * @param listaBilhetes the listaBilhetes to set
     */
    public void setListaBilhetes(List<Bilhete> listaBilhetes) {
        this.listaBilhetes = listaBilhetes;

    }
    
}
