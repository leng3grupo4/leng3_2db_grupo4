package leng3_2db_grupo4;

public class Atividade {

    private String nomeatividade;
    private String areaatividade;
    private int custosatividade;

    private static String nomeatividade_omissao = "sem nome";
    private static String areaatividade_omissao = "sem área";
    private static int custosatividade_omissao = 0;

    public Atividade(String nomeatividade, String areaatividade, int custosatividade) {

        this.nomeatividade = nomeatividade;
        this.areaatividade = areaatividade;
        this.custosatividade = custosatividade;
    }

    public Atividade() {
        nomeatividade = nomeatividade_omissao;
        areaatividade = areaatividade_omissao;
        custosatividade = custosatividade_omissao;

    }

    public String getNomeatividade() {
        return nomeatividade;
    }

    public void setNomeatividade(String nomeatividade) {
        this.nomeatividade = nomeatividade;
    }

    public String getAreaatividade() {
        return areaatividade;
    }

    public void setAreaatividade(String areaatividade) {
        this.areaatividade = areaatividade;
    }

    public int getCustosatividade() {
        return custosatividade;
    }

    public void setCustosatividade(int custosatividade) {
        this.custosatividade = custosatividade;
    }

}
