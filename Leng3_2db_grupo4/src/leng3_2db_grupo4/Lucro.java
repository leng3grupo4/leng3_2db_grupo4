
package leng3_2db_grupo4;
import java.util.ArrayList;

public class Lucro {
    private int lucrobarracas;
    private int lucrobilhetes;
    private int lucropatrocinios;
    private int lucrocamisolas;
    private int lucrototal;
    
    private static final int LUCROBARRACAS_POR_OMISSAO = 0;
    private static final int LUCROBILHETES_POR_OMISSAO = 0;
    private static final int LUCROPATROCINIOS_POR_OMISSAO = 0;
    private static final int LUCROCAMISOLAS_POR_OMISSAO = 0;
    private static final int LUCROTOTAL_POR_OMISSAO = 0;
    
    public Lucro () {
        lucrobarracas=LUCROBARRACAS_POR_OMISSAO;
        lucrobilhetes=LUCROBILHETES_POR_OMISSAO;
        lucropatrocinios=LUCROPATROCINIOS_POR_OMISSAO;
        lucrocamisolas=LUCROCAMISOLAS_POR_OMISSAO;
        lucrototal=LUCROTOTAL_POR_OMISSAO;
    }
    public Lucro(int lucrobarracas,int lucrobilhetes,int lucropatrocinios,int lucrocamisolas,int lucrototal){
        this.lucrobarracas=lucrobarracas;
        this.lucrobilhetes=lucrobilhetes;
        this.lucrocamisolas=lucrocamisolas;
        this.lucropatrocinios=lucropatrocinios;
        this.lucrototal=lucrototal;
    }

    /**
     * @return the lucrobarracas
     */
    public int getLucrobarracas() {
        return lucrobarracas;
    }

    /**
     * @param lucrobarracas the lucrobarracas to set
     */
    public void setLucrobarracas(int lucrobarracas) {
        this.lucrobarracas = lucrobarracas;
    }

    /**
     * @return the lucrobilhetes
     */
    public int getLucrobilhetes() {
        return lucrobilhetes;
    }

    /**
     * @param lucrobilhetes the lucrobilhetes to set
     */
    public void setLucrobilhetes(int lucrobilhetes) {
        this.lucrobilhetes = lucrobilhetes;
    }

    /**
     * @return the lucropatrocinios
     */
    public int getLucropatrocinios() {
        return lucropatrocinios;
    }

    /**
     * @param lucropatrocinios the lucropatrocinios to set
     */
    public void setLucropatrocinios(int lucropatrocinios) {
        this.lucropatrocinios = lucropatrocinios;
    }

    /**
     * @return the lucrocamisolas
     */
    public int getLucrocamisolas() {
        return lucrocamisolas;
    }

    /**
     * @param lucrocamisolas the lucrocamisolas to set
     */
    public void setLucrocamisolas(int lucrocamisolas) {
        this.lucrocamisolas = lucrocamisolas;
    }

    /**
     * @return the lucrototal
     */
    public int getLucrototal() {
        return lucrototal;
    }

    /**
     * @param lucrototal the lucrototal to set
     */
    public void setLucrototal(int lucrototal) {
        this.lucrototal = lucrocamisolas + lucropatrocinios + lucrobilhetes + lucrobarracas;
    }
    
    
}