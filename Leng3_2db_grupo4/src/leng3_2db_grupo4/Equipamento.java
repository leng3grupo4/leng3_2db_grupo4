/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

/**
 *
 * @author renat
 */
public enum Equipamento {
    
    BASIC("Preço: 70 mil euros\n" ,
"1 Estroboscópio 800W\n" +
"2 Alto-falante para agudos 4000W\n" +
"2 Alto-falante para graves 4000W\n" +
"1 Máquina de tempestade de raios 4000W\n" +
"1 Máquina de bolhas 250W\n" +
"1 Canhão de Espuma 2000W\n" +
"1 Pano de Fundo Enorme com logo da Banda 3000W\n" +
"1 Medium Rack 5000W\n" +
"1 Supersize Rack 7500W\n" +
"1 Projetor com telão 500W\n" +
"2 Telão de Plasma 1000W\n" +
"1 Máquina de Fumaça 500W\n" +
"1 Sistema de Controle de Luz Avançado 3000W\n" +
"1 Mesa de Mixagem Deluxe 2000W\n" +
"1 Monitor Falante Grande 500W"),
    
    STANDARD("Preço: 115 mil euros\n" ,
"2 Estroboscópio 1600W\n" +
"5 Alto-falante para agudos 10000W\n" +
"5 Alto-falante para graves 10000W\n" +
"1 Máquina de tempestade de raios 4000W\n" +
"1 Máquina de bolhas 250W\n" +
"1 Canhão de Espuma 2000W \n" +
"1 Pano de Fundo Enorme com logo da Banda 3000W\n" +
"4 Supersize Rack 30000W\n" +
"3 Projetor com telão 1500W\n" +
"2 Telão de Plasma 1000W\n" +
"1 Máquina de Fumaça 500W\n" +
"1 Sistema de Controle de Luz Avançado 3000W\n" +
"1 Mesa de Mixagem Deluxe 2000W\n" +
"3 Monitor Falante Grande 1500W"),
    
    EXCELLENCE("Preço: 130 mil euros\n" ,
"2 Estroboscópio 1600W\n" +
"8 Alto-falante para agudos 16000W\n" +
"7 Alto-falante para graves 14000W\n" +
"1 Máquina de tempestade de raios 4000W\n" +
"1 Máquina de bolhas 250W\n" +
"1 Foam Cannon 2000W\n" +
"1 Pano de Fundo Enorme com logo da Banda 3000W\n" +
"1 Medium Rack 5000W\n" +
"6 Supersize Rack 45500W\n" +
"1 Projetor com telão 500W\n" +
"2 Telão de Plasma 1000W\n" +
"1 Máquina de Fumaça 500W\n" +
"1 Sistema de Controle de Luz Avançado 3000W\n" +
"1 Mesa de Mixagem Deluxe 2000W\n" +
"1 Monitor Falante Grande 500W")
    ;

    private String pac;
    private String custo;

    /**
     * @param pac
     */
    Equipamento(String preco, String pacote) {
        pac = pacote;
        custo = preco;
    }
    
    public String getPac() {
        return pac;
    }
    
    public String getCusto() {
        return custo;
    }
    
    
}
    

