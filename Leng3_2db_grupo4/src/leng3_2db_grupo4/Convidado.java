/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

/**
 *
 * @author Asus
 */
public class Convidado {
    
    private String nome;
    private int idade;
    private String email;
    public Nacionalidade nacionalidade;
//    private final String tipoDeBilhete = "VIP";
    
    public Convidado (){
    }
    
    public Convidado (String nome, int idade, String email, Nacionalidade nacionalidade){
        setNome(nome);
        setIdade(idade);
        setEmail(email);
        setNacionalidade(nacionalidade);
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the idade
     */
    public int getIdade() {
        return idade;
    }

    /**
     * @param idade the idade to set
     */
    public void setIdade(int idade) {
        this.idade = idade;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    public Nacionalidade getNacionalidade() {
        return nacionalidade;
    }

    /**
     * @param nacionalidade
     */
    public void setNacionalidade(Nacionalidade nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    @Override
    public String toString() {
        return String.format("Nome: " + nome + " Idade: " + idade + " Email: " + email);
    }
    
}
