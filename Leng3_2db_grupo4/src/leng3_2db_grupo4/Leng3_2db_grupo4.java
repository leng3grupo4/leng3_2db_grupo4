package leng3_2db_grupo4;

import java.util.Scanner;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import leng3_2db_grupo4.ListaClientes;

public class Leng3_2db_grupo4 {

    public static void main(String[] args) {
        double lucrobilhetes = 1;
        double lucrobarracas = 1;
        double lucrocamisolas = 1;
        double lucrototal = 0;
        double lucropatrocinios = 1;
        Evento e1 = new Evento("Nome", "Hora Inicio", "Hora Fim", "Local", "Patrocinio", "Local");

//        ArrayList<Lucro> lucrobilhetes1 = new ArrayList<>();
        ArrayList<Cliente> listaClientes = new ArrayList<>();
        ArrayList<Organizador> listaOrganizadores = new ArrayList<>();
        ArrayList<Artista> listaArtistas = new ArrayList<>();
        ArrayList<Convidado> listaConvidados = new ArrayList<>();
        ArrayList<Palco> listaPalcos = new ArrayList<>();
        ArrayList<Bilhete> listaBilhetes = new ArrayList<>();
        ArrayList<Cartaz> listaCartazes = new ArrayList<>();
        ArrayList<Infraestrutura> listaInfraestruturas = new ArrayList<>();
        ArrayList<Patrocinador> listaPatrocinadores = new ArrayList<>();
        ArrayList<Merchandise> listaMerchandise= new ArrayList<>();
        
        
        Infraestrutura i1=new Infraestrutura("Coca Cola",910000000, "Zona VIP", "Restauracao", 2, 2000);
        Infraestrutura i2=new Infraestrutura("Grupo Vendap",235458777, "Todo o evento", "WC", 300, -2000);

        Palco p1 = new Palco(Palco.tipoPalco.PRINCIPAL,18888, Equipamento.EXCELLENCE);
        Palco p2 = new Palco(Palco.tipoPalco.SECUNDARIO,2000 ,Equipamento.STANDARD);
        Palco p3 = new Palco(Palco.tipoPalco.SECUNDARIO,2000 ,Equipamento.BASIC);
        Palco p4 = new Palco(Palco.tipoPalco.PRINCIPAL,2000000, Equipamento.EXCELLENCE);
        
        Artista a1 = new Artista("ASAP Rocky", p1, "Hip Hop", 1, 23);
        Artista a2 = new Artista("Xutos e Pontapes", p2,"Rock",1, 18);
        Artista a3 = new Artista("Rosinha", p3, "Pimba", 2,15);
        Artista a4 = new Artista ("Nickelback",p4,"Rock",2, 23);
        
        Cartaz d1 = new Cartaz (a1.getNome(),a2.getNome(),1);
        Cartaz d2 = new Cartaz (a3.getNome(),a4.getNome(),2);
        
        Organizador o1 = new Organizador("Jose", 1888888888, "o_meu_bm_é_melhor_que_o_teu@gmail.com", Organizador.AreaOrganizador.PROGRAMAÇAO, Organizador.Hierarquia.STAFF);
        Organizador o2 = new Organizador("Renato", 555555555, "o_rei_da_programacao@hotmail.com", Organizador.AreaOrganizador.LOGISTICA, Organizador.Hierarquia.ADMINISTRADOR);
        Organizador o3 = new Organizador("Francisco", 457878777, "a_carregar_o_grupo_as_costas@hotmail.pt", Organizador.AreaOrganizador.BILHETICA, Organizador.Hierarquia.VOLUNTARIO);
        Organizador o4 = new Organizador("Jorge", 1415151544, "sou_abusado_no_trabalho@isep.ipp.pt", Organizador.AreaOrganizador.BILHETICA, Organizador.Hierarquia.VOLUNTARIO);
        Organizador o5 = new Organizador("Pedro", 1455488878, "festivais_é_aqui_com_o_menino@myspace.com", Organizador.AreaOrganizador.PROGRAMAÇAO, Organizador.Hierarquia.STAFF);
        Organizador o6 = new Organizador("Diogo", 555888888, "fui_de_ferias_e_nem_trouxe_recordacoes@outlook.com", Organizador.AreaOrganizador.FINANCEIRO, Organizador.Hierarquia.ADMINISTRADOR);
        
        Bilhete b1 = new Bilhete(1,Bilhete.TipoBilhete.DIARIO, 100, "Acesso Diário");
        Bilhete b2 = new Bilhete(2,Bilhete.TipoBilhete.COMPLETO, 250, "Acesso Completo");
        Bilhete b3 = new Bilhete(3,Bilhete.TipoBilhete.VIP, 500, "Acesso Completo");
        Bilhete b4 = new Bilhete(4,Bilhete.TipoBilhete.COMPLETO, 250, "Acesso Completo");
        Bilhete b5 = new Bilhete(5,Bilhete.TipoBilhete.COMPLETO, 250, "Acesso Completo");
        Bilhete b6 = new Bilhete(6,Bilhete.TipoBilhete.CONVIDADO, 0, "Acesso Completo");
        Bilhete b7 = new Bilhete(7,Bilhete.TipoBilhete.VIP, 500, "Acesso Total");
        
        Cliente c1 = new Cliente("Nome1", 25, "cliente1@isep.ipp.pt", Nacionalidade.PORTUGUESA, b1);
        Cliente c2 = new Cliente("Nome2", 25, "cliente2@isep.ipp.pt", Nacionalidade.INGLESA, b2);
        Cliente c3 = new Cliente("Nome3", 25, "cliente3@isep.ipp.pt", Nacionalidade.ALEMA, b3);
        Cliente c4 = new Cliente("Nome4", 25, "cliente1@isep.ipp.pt", Nacionalidade.PORTUGUESA, b1);
        Cliente c5 = new Cliente("Nome5", 25, "cliente3@isep.ipp.pt", Nacionalidade.OUTRA, b3);
        Cliente c6 = new Cliente("Nome6", 25, "cliente1@isep.ipp.pt", Nacionalidade.PORTUGUESA, b1);
        Cliente c7 = new Cliente("Nome7", 25, "cliente3@isep.ipp.pt", Nacionalidade.ALEMA, b3);
        Cliente c8 = new Cliente("Nome8", 25, "cliente1@isep.ipp.pt", Nacionalidade.ITALIANA, b1);

        Convidado g1 = new Convidado("Nome1", 30, "convidado@isep.ipp.pt", Nacionalidade.PORTUGUESA);
        Convidado g2 = new Convidado("Nome2", 30, "convidado@isep.ipp.pt", Nacionalidade.AMERICANA);
        Convidado g3 = new Convidado("Nome3", 30, "convidado@isep.ipp.pt", Nacionalidade.AMERICANA);
        Convidado g4 = new Convidado("Nome4", 30, "convidado@isep.ipp.pt", Nacionalidade.ESPANHOLA);
        Convidado g5 = new Convidado("Nome5", 30, "convidado@isep.ipp.pt", Nacionalidade.OUTRA);
        
        Patrocinador t1 = new Patrocinador("NOS", 2000000);
        Patrocinador t2 = new Patrocinador("STCP", 5000);
        Patrocinador t3 = new Patrocinador("Super-Bock", 250000);
        Patrocinador t4 = new Patrocinador("ISEP", 10);
        
        Merchandise m1 = new Merchandise (20000, 800000);
        
//        Lucro lucro = new Lucro(Bilhetica.calcularLucroBilhetica(listaBilhetes), lucrobarracas, lucrocamisolas, lucrototal, lucropatrocinios);


        listaInfraestruturas.add(i1);
        listaInfraestruturas.add(i2);
        
        listaPalcos.add(p1);
        listaPalcos.add(p2);
        listaPalcos.add(p3);
        listaPalcos.add(p4);
        
        listaOrganizadores.add(o1);
        listaOrganizadores.add(o2);
        listaOrganizadores.add(o3);
        listaOrganizadores.add(o4);
        listaOrganizadores.add(o5);
        listaOrganizadores.add(o6);
        
        listaArtistas.add(a1);
        listaArtistas.add(a2);
        listaArtistas.add(a3);
        listaArtistas.add(a4);
       
        listaCartazes.add(d1);
        listaCartazes.add(d2);
        
        listaClientes.add(c1);
        listaClientes.add(c2);
        listaClientes.add(c3);
        listaClientes.add(c4);
        listaClientes.add(c5);
        listaClientes.add(c6);
        listaClientes.add(c7);
        listaClientes.add(c8);

        listaConvidados.add(g1);
        listaConvidados.add(g2);
        listaConvidados.add(g3);
        listaConvidados.add(g4);
        listaConvidados.add(g5);
        
        listaBilhetes.add(b1);
        listaBilhetes.add(b2);
        listaBilhetes.add(b3);
        listaBilhetes.add(b4);
        listaBilhetes.add(b5);
        listaBilhetes.add(b6);
        listaBilhetes.add(b7);
        
        listaPatrocinadores.add(t1);
        listaPatrocinadores.add(t4);
        listaPatrocinadores.add(t2);
        listaPatrocinadores.add(t3);
        
        listaMerchandise.add(m1);

        System.out.println(e1);
        System.out.println("###Lista de Clientes:###\n"+listaClientes+"\n");
        System.out.println("\n###Lista de Palcos:###\n"+listaPalcos+"\n");
        System.out.println("\n###Lista de Artistas:###\n"+listaArtistas+"\n");
        System.out.println("\n###Cartaz###:\n"+listaCartazes+"\n");
        System.out.println("\n###Lista de Convidados:###\n"+listaConvidados+"\n");
        System.out.println("\n###Lista de Organizadores:###\n"+listaOrganizadores+"\n");
        System.out.println("\n###Lista de Infraestruturas:###\n"+listaInfraestruturas+"\n");
        
        System.out.println("\n###Informações Adicionais:###");
        Bilhetica.contarTipoDeBilhetes(listaBilhetes); //Mudei o metodo para a classe Bilhetica, falta verificar se deu problema
        Bilhetica.contarListaBilhetes(listaBilhetes);
        Bilhetica.calcularLucroBilhetica(listaBilhetes);
        System.out.println("\n###Lucro Patrocinador:###\n"+listaPatrocinadores+"\n");
        System.out.println("\n###Lucro Merchandise:###\n"+listaMerchandise+"\n");
        
        ListaClientes.contarNacionalidades(listaClientes);
        ListaConvidados.contarNacionalidades(listaConvidados);
        ListaOrganizadores.contarTipoOrganizadores(listaOrganizadores);
        
        

    }
}
