/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

/**
 *
 * @author José
 */
public class Organizador {



    
    public enum AreaOrganizador{
        BILHETICA, FINANCEIRO, PROGRAMAÇAO, LOGISTICA}
    
    public enum Hierarquia{ 
        VOLUNTARIO, STAFF, ADMINISTRADOR, CHEFE}
    
    private String nome;
    private AreaOrganizador areaOrganizador;
    private Hierarquia hierarquia;
    private int NIF;
    private String email;
    
     
    public Organizador (String nome, int NIF, String email, AreaOrganizador areaOrganizador, Hierarquia hierarquia){
       this.nome=nome;
       this.NIF=NIF;
       this.email=email;
       setAreaOrganizador(areaOrganizador);
       setHierarquia(hierarquia);
       
    }

  /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the NIF
     */
    public int getNIF() {
        return NIF;
    }

    /**
     * @param NIF the NIF to set
     */
    public void setNIF(int NIF) {
        this.NIF = NIF;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
        /**
     * @return the areaOrganizador
     */
    public AreaOrganizador getAreaOrganizador() {
        return areaOrganizador;
    }

    /**
     * @param areaOrganizador the areaOrganizador to set
     */
    public void setAreaOrganizador(AreaOrganizador areaOrganizador) {
        this.areaOrganizador = areaOrganizador;
    }

    /**
     * @return the hierarquia
     */
    public Hierarquia getHierarquia() {
        return hierarquia;
    }

    /**
     * @param hierarquia the hierarquia to set
     */
    public void setHierarquia(Hierarquia hierarquia) {
        this.hierarquia = hierarquia;
    }
 
   
  public String toString() {
        return String.format("\n \nNome: %s \nNIF: %08d \nEmail: %s \nHierarquia: %s \nArea: %s", getNome(), getNIF(), getEmail(), getHierarquia(), getAreaOrganizador());
    }
    
}
