/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

/**
 *
 * @author Asus
 */
public class BilheteiraFisica {
    
    private int nrBilheteira;
    private String local;
    private String horario;

    /**
     * @return the nrBilheteira
     */
    public int getNrBilheteira() {
        return nrBilheteira;
    }

    /**
     * @param nrBilheteira the nrBilheteira to set
     */
    public void setNrBilheteira(int nrBilheteira) {
        this.nrBilheteira = nrBilheteira;
    }

    /**
     * @return the Local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param Local the Local to set
     */
    public void setLocal(String Local) {
        this.local = Local;
    }

    /**
     * @return the Horario
     */
    public String getHorario() {
        return horario;
    }

    /**
     * @param Horario the Horario to set
     */
    public void setHorario(String Horario) {
        this.horario = Horario;
    }
    
    @Override
     public String toString() {
        return String.format("Bilheteira Nº" + nrBilheteira + "  Local:" + local + "." + horario);
    }
}
