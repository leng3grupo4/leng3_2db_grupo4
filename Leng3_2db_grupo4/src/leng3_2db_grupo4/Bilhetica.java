/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author renat
 */
public class Bilhetica {
    
    public static void contarListaBilhetes(List<Bilhete> listaBilhetes) {

        System.out.println("Nº Bilhetes: " + listaBilhetes.size());

    }
    
    public static void contarTipoDeBilhetes(List<Bilhete> listaBilhetes) {
        Map<Bilhete.TipoBilhete, Integer> map = new HashMap<>();
        
        int contadorBilheteDiario = 0;
        int contadorBilheteCompleto = 0;
        int contadorBilheteVIP = 0;
        int contadorBilheteConvidado = 0;

//        System.out.println(listaBilhetes);

        for (int i = 0; i < listaBilhetes.size(); i++) {
            if (listaBilhetes.get(i).getTipoDeBilhete() != null) {

                // em vez do switch fazer um for sobre Nacionalidade.values()
                // for(int j = 0; j < Nacionalidade.values(); j++)
                switch (listaBilhetes.get(i).getTipoDeBilhete()) {
                    case DIARIO:
                        contadorBilheteDiario++;
                        map.put(Bilhete.TipoBilhete.DIARIO, contadorBilheteDiario);
                        break;
                    case COMPLETO:
                        contadorBilheteCompleto++;
                        map.put(Bilhete.TipoBilhete.COMPLETO, contadorBilheteCompleto);
                        break;
                    case VIP:
                        contadorBilheteVIP++;
                        map.put(Bilhete.TipoBilhete.VIP, contadorBilheteVIP);
                        break;
                    case CONVIDADO:
                        contadorBilheteConvidado++;
                        map.put(Bilhete.TipoBilhete.CONVIDADO, contadorBilheteConvidado);
                        break;
                    default:
                        break;
                }
            }
        }
        
        System.out.println("\n \nBilhetes Vendidos: \nNº Bilhete Diario: "+contadorBilheteDiario+"\nNº Bilhete Completo: "+contadorBilheteCompleto+"\nNº Bilhete VIP: "+contadorBilheteVIP+"\nNº Bilhete Convidado: "+contadorBilheteConvidado);
        
    }
    
    public static void calcularLucroBilhetica(List<Bilhete> listaBilhetes) {
        Map<Bilhete.TipoBilhete, Integer> map = new HashMap<>();
        
        double lucroBilhetes = 0;
        int contadorBilheteDiario = 0;
        int contadorBilheteCompleto = 0;
        int contadorBilheteVIP = 0;
        int contadorBilheteConvidado = 0;
        int contadorLucroTotal=0;


//        System.out.println(listaBilhetes);

        for (int i = 0; i < listaBilhetes.size(); i++) {
            if (listaBilhetes.get(i).getTipoDeBilhete() != null) {

                switch (listaBilhetes.get(i).getTipoDeBilhete()) {
                    case DIARIO:
                        contadorBilheteDiario++;
                        map.put(Bilhete.TipoBilhete.DIARIO, contadorBilheteDiario);
                        break;
                    case COMPLETO:
                        contadorBilheteCompleto++;
                        map.put(Bilhete.TipoBilhete.COMPLETO, contadorBilheteCompleto);
                        break;
                    case VIP:
                        contadorBilheteVIP++;
                        map.put(Bilhete.TipoBilhete.VIP, contadorBilheteVIP);
                        break;
                    case CONVIDADO:
                        contadorBilheteConvidado++;
                        map.put(Bilhete.TipoBilhete.CONVIDADO, contadorBilheteConvidado);
                        break;
                    default:
                        break;
                }

            }
             contadorLucroTotal=contadorBilheteVIP*500+contadorBilheteCompleto*250+contadorBilheteDiario*100;

        } System.out.println("\nLucros Bilhetica:"
                + "\nLucro Bilhetes Diarios: "+ contadorBilheteDiario*100+""
                + "\nLucro Bilhetes Completos: "+ contadorBilheteCompleto*250+""
                + "\nLucro Bilhetes VIP: "+ contadorBilheteVIP*500+""
                + "\nLucro Total Bilhetica: "+contadorLucroTotal);
    }
    
}
