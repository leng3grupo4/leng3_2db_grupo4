/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

/**
 *
 * @author jose
 */
public class Patrocinador {
    
    private String nome;
    private int valorPatrocinado;
    
    public Patrocinador (String nome, int valorPatrocinado){
        this.nome=nome;
        this.valorPatrocinado=valorPatrocinado;
        
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the ValorPatrocinado
     */
    public int getValorPatrocinado() {
        return valorPatrocinado;
    }

    /**
     * @param ValorPatrocinado the ValorPatrocinado to set
     */
    public void setValorPatrocinado(int ValorPatrocinado) {
        this.valorPatrocinado = ValorPatrocinado;
    }
    
     @Override
    public String toString() {
        return String.format("\n \nNome de Patrocinador: %s \nValor Patrocinado: %d", getNome(), getValorPatrocinado()) ;
    }

    
}
