/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class ListaArtistas {
       private List<Artista> listaArtistas;    
    
    public ListaArtistas() {  
        listaArtistas = new ArrayList<>();
    }

    public boolean adicionarArtista(Artista artistas) {
        if(!listaArtistas.contains(artistas)) {
            return listaArtistas.add(artistas);
        }
        
        return false;
    }
    
    public boolean guardar(String nomeFicheiro) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(listaArtistas);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    } 
    
    public boolean ler(String nomeFicheiro) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(nomeFicheiro));
            try {
                listaArtistas=(List<Artista>)in.readObject();
            } finally {
                in.close();
            }
            return true;
        } catch (IOException|ClassNotFoundException ex) {
            return false;
        }
    }
    
    /**
     * @return the listaConvidados
     */
    public List<Artista> getListaClientes() {
        return listaArtistas;
    }

    /**
     * @param listaArtistas the listaConvidados to set
     */
    public void setListaArtistas(List<Artista> listaArtistas) {
        this.listaArtistas = listaArtistas;
        
    }
    
}
