/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

/**
 *
 * @author Diogo
 */
public class Gasto {

    private double gastosagua;
    private double gastosluz;
    private double gastosconstrucao;
    private double aluguerequipamentos;
    private double gastosartistas;
    private double gastosmarketing;
    private double aluguerlocal;
    private double gastosseguranca;
    private double gastoscuidadosmedicos;
    private double gastostransportes;
    private double gastosseguros;
    private double gastosgeral;

    private static final double GASTOSAGUA_POR_OMISSAO = 0;
    private static final double GASTOSLUZ_POR_OMISSAO = 0;
    private static final double GASTOSCONSTRUCAO_POR_OMISSAO = 0;
    private static final double ALUGUEREQUIPAMENTOS_POR_OMISSAO = 0;
    private static final double GASTOSARTISTAS_POR_OMISSAO = 0;
    private static final double GASTOSMARKETING_POR_OMISSAO = 0;
    private static final double ALUGUERLOCAL_POR_OMISSAO = 0;
    private static final double GASTOSSEGURANCA_POR_OMISSAO = 0;
    private static final double GASTOSCUIDADOSMEDICOS_POR_OMISSAO = 0;
    private static final double GASTOSTRANSPORTES_POR_OMISSAO = 0;
    private static final double GASTOSSEGUROS_POR_OMISSAO = 0;
    private static final double GASTOSGERAL_POR_OMISSAO = 0;

    public Gasto() {
        gastosagua = GASTOSAGUA_POR_OMISSAO;
        gastosluz = GASTOSLUZ_POR_OMISSAO;
        gastosconstrucao = GASTOSCONSTRUCAO_POR_OMISSAO;
        aluguerequipamentos = ALUGUEREQUIPAMENTOS_POR_OMISSAO;
        gastosartistas = GASTOSARTISTAS_POR_OMISSAO;
        gastosmarketing = GASTOSMARKETING_POR_OMISSAO;
        aluguerlocal = ALUGUERLOCAL_POR_OMISSAO;
        gastosseguranca = GASTOSSEGURANCA_POR_OMISSAO;
        gastoscuidadosmedicos = GASTOSCUIDADOSMEDICOS_POR_OMISSAO;
        gastostransportes = GASTOSTRANSPORTES_POR_OMISSAO;
        gastosseguros = GASTOSSEGUROS_POR_OMISSAO;
        gastosgeral = GASTOSGERAL_POR_OMISSAO;

    }
    public Gasto(double gastosagua, double gastosluz, double gastosconstrucao, double aluguerequipamentos, double gastosartistas, double gastosmarketing, double aluguerlocal, double gastosseguranca, double gastoscuidadosmedicos, double gastostransportes,double gastosseguros, double gastosgeral){
        this.aluguerequipamentos=aluguerequipamentos;
        this.aluguerlocal=aluguerlocal;
        this.gastosagua=gastosagua;
        this.gastosartistas=gastosartistas;
        this.gastosconstrucao=gastosconstrucao;
        this.gastoscuidadosmedicos=gastoscuidadosmedicos;
        this.gastosluz=gastosluz;
        this.gastosmarketing=gastosmarketing;
        this.gastosseguranca=gastosseguranca;
        this.gastosseguros=gastosseguros;
        this.gastostransportes=gastostransportes;
        this.gastosgeral=gastosgeral;
    }

    /**
     * @return the gastosagua
     */
    public double getGastosagua() {
        return gastosagua;
    }

    /**
     * @param gastosagua the gastosagua to set
     */
    public void setGastosagua(double gastosagua) {
        this.gastosagua = gastosagua;
    }

    /**
     * @return the gastosluz
     */
    public double getGastosluz() {
        return gastosluz;
    }

    /**
     * @param gastosluz the gastosluz to set
     */
    public void setGastosluz(double gastosluz) {
        this.gastosluz = gastosluz;
    }

    /**
     * @return the gastosconstrucao
     */
    public double getGastosconstrucao() {
        return gastosconstrucao;
    }

    /**
     * @param gastosconstrucao the gastosconstrucao to set
     */
    public void setGastosconstrucao(double gastosconstrucao) {
        this.gastosconstrucao = gastosconstrucao;
    }

    /**
     * @return the aluguerequipamentos
     */
    public double getAluguerequipamentos() {
        return aluguerequipamentos;
    }

    /**
     * @param aluguerequipamentos the aluguerequipamentos to set
     */
    public void setAluguerequipamentos(double aluguerequipamentos) {
        this.aluguerequipamentos = aluguerequipamentos;
    }

    /**
     * @return the gastosartistas
     */
    public double getGastosartistas() {
        return gastosartistas;
    }

    /**
     * @param gastosartistas the gastosartistas to set
     */
    public void setGastosartistas(double gastosartistas) {
        this.gastosartistas = gastosartistas;
    }

    /**
     * @return the gastosmarketing
     */
    public double getGastosmarketing() {
        return gastosmarketing;
    }

    /**
     * @param gastosmarketing the gastosmarketing to set
     */
    public void setGastosmarketing(double gastosmarketing) {
        this.gastosmarketing = gastosmarketing;
    }

    /**
     * @return the aluguerlocal
     */
    public double getAluguerlocal() {
        return aluguerlocal;
    }

    /**
     * @param aluguerlocal the aluguerlocal to set
     */
    public void setAluguerlocal(double aluguerlocal) {
        this.aluguerlocal = aluguerlocal;
    }

    /**
     * @return the gastosseguranca
     */
    public double getGastosseguranca() {
        return gastosseguranca;
    }

    /**
     * @param gastosseguranca the gastosseguranca to set
     */
    public void setGastosseguranca(double gastosseguranca) {
        this.gastosseguranca = gastosseguranca;
    }

    /**
     * @return the gastoscuidadosmedicos
     */
    public double getGastoscuidadosmedicos() {
        return gastoscuidadosmedicos;
    }

    /**
     * @param gastoscuidadosmedicos the gastoscuidadosmedicos to set
     */
    public void setGastoscuidadosmedicos(double gastoscuidadosmedicos) {
        this.gastoscuidadosmedicos = gastoscuidadosmedicos;
    }

    /**
     * @return the gastostransportes
     */
    public double getGastostransportes() {
        return gastostransportes;
    }

    /**
     * @param gastostransportes the gastostransportes to set
     */
    public void setGastostransportes(double gastostransportes) {
        this.gastostransportes = gastostransportes;
    }

    /**
     * @return the gastosseguros
     */
    public double getGastosseguros() {
        return gastosseguros;
    }

    /**
     * @param gastosseguros the gastosseguros to set
     */
    public void setGastosseguros(double gastosseguros) {
        this.gastosseguros = gastosseguros;
    }

    /**
     * @return the gastosgeral
     */
    public double getGastosgeral() {
        return gastosgeral;
    }

    /**
     * @param gastosgeral the gastosgeral to set
     */
    public void setGastosgeral(double gastosgeral) {
        this.gastosgeral = gastosagua + gastosluz + gastosconstrucao + aluguerequipamentos + gastosartistas + gastosmarketing + aluguerlocal + gastosseguranca + gastoscuidadosmedicos + gastostransportes + gastosseguros;   
    }

}
