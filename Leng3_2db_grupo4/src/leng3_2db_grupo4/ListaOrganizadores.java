/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Asus
 */
public class ListaOrganizadores {

    private List<Organizador> listaOrganizadores;

    public ListaOrganizadores() {
        listaOrganizadores = new ArrayList<>();
    }

    public boolean adicionarBilhete(Organizador Organizador) {
        if (!listaOrganizadores.contains(Organizador)) {
            return listaOrganizadores.add(Organizador);
        }

        return false;
    }

    public boolean guardar(String nomeFicheiro) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(listaOrganizadores);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean ler(String nomeFicheiro) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(nomeFicheiro));
            try {
                listaOrganizadores = (List<Organizador>) in.readObject();
            } finally {
                in.close();
            }
            return true;
        } catch (IOException | ClassNotFoundException ex) {
            return false;
        }
    }

    /**
     * @return the listaBilhetes
     */
    public List<Organizador> getlistaOrganizadores() {
        return listaOrganizadores;
    }

    /**
     * @param listaOrganizadores the listaBilhetes to set
     */
    public void setListaBilhetes(List<Organizador> listaOrganizadores) {
        this.listaOrganizadores = listaOrganizadores;

    }

    public static void contarlistaOrganizadores(List<Organizador> listaOrganizadores) {

        System.out.println("NrÂº Bilhetes: " + listaOrganizadores.size());

    }

    public static void contarTipoOrganizadores(List<Organizador> listaOrganizadores) {
        Map<Organizador.Hierarquia, Integer> map = new HashMap<>();

        int contadorVoluntario = 0;
        int contadorStaff = 0;
        int contadorAdministrador = 0;
        int contadorChefe = 0;

//        System.out.println(listaOrganizadores);

        for (int i = 0; i < listaOrganizadores.size(); i++) {
            if (listaOrganizadores.get(i).getHierarquia() != null) {

                // em vez do switch fazer um for sobre Nacionalidade.values()
                // for(int j = 0; j < Nacionalidade.values(); j++)
                switch (listaOrganizadores.get(i).getHierarquia()) {
                    case VOLUNTARIO:
                        contadorVoluntario++;
                        map.put(Organizador.Hierarquia.VOLUNTARIO, contadorVoluntario);
                        break;
                    case STAFF:
                        contadorStaff++;
                        map.put(Organizador.Hierarquia.STAFF, contadorStaff);
                        break;
                    case ADMINISTRADOR:
                        contadorAdministrador++;
                        map.put(Organizador.Hierarquia.ADMINISTRADOR, contadorAdministrador);
                        break;
                    case CHEFE:
                        contadorChefe++;
                        map.put(Organizador.Hierarquia.CHEFE, contadorChefe);
                        break;
                    default:
                        break;
                }
            }
        }

        System.out.println("\n \nNº de Organizadores: "
                + "\nNº Voluntarios: "+contadorVoluntario+"\nNº Staff: "+contadorStaff+"\nNº Administrador: "+contadorAdministrador+"\nNº Chefe: "+contadorChefe);

    }

}
