/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

/**
 *
 * @author Asus
 */
public class Cliente {

    private String nome;
    private int idade;
    private String email;
    private Bilhete bilhete;

    private Nacionalidade nacionalidade;
    //private int quant[];

    public Cliente() {

    }

    public Cliente(String nome, int idade, String email, Nacionalidade nacionalidade, Bilhete bilhete) {
        setNome(nome);
        setIdade(idade);
        setEmail(email);
        setNacionalidade(nacionalidade);
        setBilhete(bilhete);
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the idade
     */
    public int getIdade() {
        return idade;
    }

    /**
     * @param idade the idade to set
     */
    public void setIdade(int idade) {
        this.idade = idade;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the nacionalidade
     */
    public Nacionalidade getNacionalidade() {
        return nacionalidade;
    }

    /**
     * @param nacionalidade
     */
    public void setNacionalidade(Nacionalidade nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    @Override
    public String toString() {
        return String.format("\n \nNome: " + nome + "\nIdade: " + idade + "\nEmail: " + email);
    }

    /**
     * @return the bilhete
     */
    public Bilhete getBilhete() {
        return bilhete;
    }

    /**
     * @param bilhete the bilhete to set
     */
    public void setBilhete(Bilhete bilhete) {
        this.bilhete = bilhete;
    }

}
