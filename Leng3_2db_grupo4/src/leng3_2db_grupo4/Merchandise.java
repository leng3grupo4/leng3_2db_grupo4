/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;



public class Merchandise {
    private int produtosVendidos;
    private int LucroTotal;
    public Merchandise (int produtosVendidos, int LucroTotal) {
        this.produtosVendidos=produtosVendidos;
        this.LucroTotal=LucroTotal;


}

    /**
     * @return the produtosVendidos
     */
    public int getprodutosVendidos() {
        return produtosVendidos;
    }

    /**
     * @param produtosVendidos the produtosVendidos to set
     */
    public void setprodutosVendidos(int produtosVendidos) {
        this.produtosVendidos = produtosVendidos;
    }

    /**
     * @return the LucroTotal
     */
    public int getLucroTotal() {
        return LucroTotal;
    }

    /**
     * @param LucroTotal the LucroTotal to set
     */
    public void setLucroTotal(int LucroTotal) {
        this.LucroTotal = LucroTotal;
    }
    @Override
    public String toString() {
        return String.format("\n \nCamisolas Vendidas: %s \nValor Arrecadado: %d", getprodutosVendidos(), getLucroTotal()) ;
    }
    
}

