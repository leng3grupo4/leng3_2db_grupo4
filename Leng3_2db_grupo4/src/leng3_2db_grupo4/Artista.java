
package leng3_2db_grupo4;

public class Artista {
    
    private String nome;
    private Palco palco;
    private String generoMusical;
    private int diaConcerto;
    private int horaConcerto;
    
    private String NOME_POR_OMISSAO = "sem nome";
    private String GENERO_MUSICAL_POR_OMISSAO = "sem genero musical";
    
    
     public Artista (String nome, Palco palco, String generoMusical, int diaConcerto, int horaConcerto)  {
        this.nome = nome;
        this.palco =  palco;
        this.generoMusical  = generoMusical;
        this.diaConcerto = diaConcerto;
        this.horaConcerto = horaConcerto;
    
}
     

     
  public Artista() {
       
    }     

    
    public String getNome() {
        return nome;
    }

    
    public void setNome(String nome) {
        this.nome = nome;
    }

    
    public Palco getPalco() {
        return palco;
    }

    
    public void setPalco(Palco palco) {
        this.palco = palco;
    }

    
    public String getGeneroMusical() {
        return generoMusical;
    }

     
    public void setGeneromusical(String generoMusical) {
        this.generoMusical = generoMusical;
    } 

    /**
     * @return the diaConcerto
     */
    public int getDiaConcerto() {
        return diaConcerto;
    }

    /**
     * @param diaConcerto the diaConcerto to set
     */
    public void setDiaConcerto(int diaConcerto) {
        this.diaConcerto = diaConcerto;
    }

    /**
     * @return the horaConcerto
     */
    public int getHoraConcerto() {
        return horaConcerto;
    }

    /**
     * @param horaConcerto the horaConcerto to set
     */
    public void setHoraConcerto(int horaConcerto) {
        this.horaConcerto = horaConcerto;
    }
     
        public String toString(){
        return String.format("\n\n"+getNome()+"\nGenero musical:"+getGeneroMusical()+"\nDia do concerto:"+getDiaConcerto()+"\nHora do Concerto:"+getHoraConcerto()+getPalco()); 
        
    } 
}