/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

/**
 *
 * @author renat
 */
public class Infraestrutura {
    
    private String nomeInfraestrutura;
    private String localInfraestrutura;
    private String especialidade;
    private int numeroBarracas;
    private double taxaAluguer;
    private int numeroTelefone;
    
    private static final String NOME_NEGOCIO_POR_OMISSAO="sem nome";
    private static final String LOCAL_NEGOCIO_POR_OMISSAO="sem local";
    private static final String ESPECIALIDADE_POR_OMISSAO="sem especialidade";
    private static final int NUMERO_BARRACAS_POR_OMISSAO=0;
    private static final double TAXA_ALUGUER_POR_OMISSAO=0;
    private static final double NUMERO_TELEFONE_POR_OMISSAO=0;
    
    
    public Infraestrutura (){
        nomeInfraestrutura=NOME_NEGOCIO_POR_OMISSAO;
        localInfraestrutura=LOCAL_NEGOCIO_POR_OMISSAO;
        especialidade=ESPECIALIDADE_POR_OMISSAO;
        numeroBarracas=NUMERO_BARRACAS_POR_OMISSAO;
        taxaAluguer=TAXA_ALUGUER_POR_OMISSAO;
    }
    
    public Infraestrutura (String nomeNegocio, int numeroTelefone, String localNegocio, String especialidade, int numeroBarracas, double taxaAluguer){
        this.nomeInfraestrutura=nomeNegocio;
        this.localInfraestrutura=localNegocio;
        this.especialidade=especialidade;
        this.numeroBarracas=numeroBarracas;
        this.taxaAluguer=taxaAluguer;
        this.numeroTelefone=numeroTelefone;
    }

    /**
     * @return the especialidade
     */
    public String getEspecialidade() {
        return especialidade;
    }

    /**
     * @param especialidade the especialidade to set
     */
    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    /**
     * @return the numeroBarracas
     */
    public int getNumeroBarracas() {
        return numeroBarracas;
    }

    /**
     * @param numeroBarracas the numeroBarracas to set
     */
    public void setNumeroBarracas(int numeroBarracas) {
        this.numeroBarracas = numeroBarracas;
    }
    
    /**
     * @return the taxaAluguer
     */
    public double getTaxaAluguer() {
        return taxaAluguer;
    }

    /**
     * @param taxaAluguer the taxaAluguer to set
     */
    public void setTaxaAluguer(double taxaAluguer) {
        this.taxaAluguer = taxaAluguer;
    }

    /**
     * @return the nomeInfraestrutura
     */
    public String getNomeInfraestrutura() {
        return nomeInfraestrutura;
    }

    /**
     * @param nomeInfraestrutura the nomeInfraestrutura to set
     */
    public void setNomeInfraestrutura(String nomeInfraestrutura) {
        this.nomeInfraestrutura = nomeInfraestrutura;
    }

    /**
     * @return the localInfraestrutura
     */
    public String getLocalInfraestrutura() {
        return localInfraestrutura;
    }

    /**
     * @param localInfraestrutura the localInfraestrutura to set
     */
    public void setLocalInfraestrutura(String localInfraestrutura) {
        this.localInfraestrutura = localInfraestrutura;
    }

    /**
     * @return the numeroTelefone
     */
    public int getNumeroTelefone() {
        return numeroTelefone;
    }

    /**
     * @param numeroTelefone the numeroTelefone to set
     */
    public void setNumeroTelefone(int numeroTelefone) {
        this.numeroTelefone = numeroTelefone;
    }
    
    
   @Override
    public String toString(){
        return String.format("\n \nNome: %s \nNumero de Telefone: %09d \nLocal: %s \nEspecialidade: %s \nNumero de Barracas: %02d \nTaxa de Aluguer: %04f", getNomeInfraestrutura(), getNumeroTelefone(), getLocalInfraestrutura(), getEspecialidade(), getNumeroBarracas(), getTaxaAluguer());
        
    }


}
