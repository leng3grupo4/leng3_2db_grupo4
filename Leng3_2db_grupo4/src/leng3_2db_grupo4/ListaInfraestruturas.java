/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author renat
 */
public class ListaInfraestruturas {
       private List<Artista> listaInfraestruturas;    
    
    public ListaInfraestruturas() {  
        listaInfraestruturas = new ArrayList<>();
    }

    public boolean adicionarInfraestruturas(Artista infraestruturas) {
        if(!listaInfraestruturas.contains(infraestruturas)) {
            return listaInfraestruturas.add(infraestruturas);
        }
        
        return false;
    }
    
    public boolean guardar(String nomeFicheiro) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(listaInfraestruturas);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    } 
    
    public boolean ler(String nomeFicheiro) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(nomeFicheiro));
            try {
                listaInfraestruturas=(List<Artista>)in.readObject();
            } finally {
                in.close();
            }
            return true;
        } catch (IOException|ClassNotFoundException ex) {
            return false;
        }
    }
    
    /**
     * @return the listaInfraestruturas
     */
    public List<Artista> getListaInfraestruturas() {
        return listaInfraestruturas;
    }

    /**
     * @param listaInfraestruturas the listaInfraestruturas to set
     */
    public void setListaInfraestruturas(List<Artista> listaInfraestruturas) {
        this.listaInfraestruturas = listaInfraestruturas;
        
    }
    
}
