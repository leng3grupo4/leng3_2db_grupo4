/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jdk.nashorn.internal.objects.NativeArray;

/**
 *
 * @author Asus
 */
public class ListaClientes {

    private List<Cliente> listaClientes;

    public ListaClientes() {
        listaClientes = new ArrayList<>();
    }

    public boolean adicionarCliente(Cliente cliente) {
        if (!listaClientes.contains(cliente)) {
            return listaClientes.add(cliente);
        }

        return false;
    }

    public boolean guardar(String nomeFicheiro) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(listaClientes);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean ler(String nomeFicheiro) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(nomeFicheiro));
            try {
                listaClientes = (List<Cliente>) in.readObject();
            } finally {
                in.close();
            }
            return true;
        } catch (IOException | ClassNotFoundException ex) {
            return false;
        }
    }

    /**
     * @return the listaClientes
     */
    public List<Cliente> getListaClientes() {
        return listaClientes;
    }

    /**
     * @param listaClientes the listaClientes to set
     */
    public void setListaClientes(List<Cliente> listaClientes) {
        this.listaClientes = listaClientes;

    }

    public static void contarNacionalidades(List<Cliente> listaClientes) {
        Map<Nacionalidade, Integer> map = new HashMap<>();

        int contadorAlema = 0;
        int contadorAmericana = 0;
        int contadorPortuguesa = 0;
        int contadorEspanhola = 0;
        int contadorInglesa = 0;
        int contadorFrancesa = 0;
        int contadorItaliana = 0;
        int contadorOutra = 0;

//        System.out.println(listaClientes);

        for (int i = 0; i < listaClientes.size(); i++) {
            if (listaClientes.get(i).getNacionalidade() != null) {

                // em vez do switch fazer um for sobre Nacionalidade.values()
                // for(int j = 0; j < Nacionalidade.values(); j++)
                switch (listaClientes.get(i).getNacionalidade()) {
                    case ALEMA:
                        contadorAlema++;
                        map.put(Nacionalidade.ALEMA, contadorAlema);
                        break;
                    case AMERICANA:
                        contadorAmericana++;
                        map.put(Nacionalidade.AMERICANA, contadorAmericana);
                        break;
                    case PORTUGUESA:
                        contadorPortuguesa++;
                        map.put(Nacionalidade.PORTUGUESA, contadorPortuguesa);
                        break;
                    case INGLESA:
                        contadorInglesa++;
                        map.put(Nacionalidade.INGLESA, contadorInglesa);
                        break;
                    case ITALIANA:
                        contadorItaliana++;
                        map.put(Nacionalidade.ITALIANA, contadorItaliana);
                        break;
                    case FRANCESA:
                        contadorFrancesa++;
                        map.put(Nacionalidade.FRANCESA, contadorFrancesa);
                        break;
                    case OUTRA:
                        contadorOutra++;
                        map.put(Nacionalidade.OUTRA, contadorOutra);
                        break;
                    case ESPANHOLA:
                        contadorEspanhola++;
                        map.put(Nacionalidade.ESPANHOLA, contadorEspanhola);
                        break;
                    default:
                        break;
                }
            }
        }

        System.out.println("\n \nNacionalidades dos Clientes:");
        int estrangeiros = contadorAlema + contadorAmericana + contadorEspanhola + contadorFrancesa + contadorInglesa + contadorItaliana + contadorOutra;
        System.out.println(map);
        System.out.println("Estrangeiros: " + estrangeiros + "----> nacionais: " + contadorPortuguesa);
    }
}
