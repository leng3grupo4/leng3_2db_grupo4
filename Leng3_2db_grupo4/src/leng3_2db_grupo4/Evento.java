/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

/**
 *
 * @author José
 */
public class Evento {
    
    private String nome;
    private String HoraInicio;
    private String HoraFim;
    private String Local;
    private String Patrocinios;
    private String Cartaz;

    /**
     * @return the Nome
     */
    public Evento(String Nome, String HoraInicio, String HoraFim, String Local, String Patrocinios, String Cartaz){
    
// Fazer get's e set's aqui mesmo do Evento
    }
    public String getNome() {
        return nome;
    }

    /**
     * @param Nome the Nome to set
     */
    public void setNome(String Nome) {
        this.nome = Nome;
    }

    /**
     * @return the HoraInicio
     */
    public String getHoraInicio() {
        return HoraInicio;
    }

    /**
     * @param HoraInicio the HoraInicio to set
     */
    public void setHoraInicio(String HoraInicio) {
        this.HoraInicio = HoraInicio;
    }

    /**
     * @return the HoraFim
     */
    public String getHoraFim() {
        return HoraFim;
    }

    /**
     * @param HoraFim the HoraFim to set
     */
    public void setHoraFim(String HoraFim) {
        this.HoraFim = HoraFim;
    }

    /**
     * @return the Local
     */
    public String getLocal() {
        return Local;
    }

    /**
     * @param Local the Local to set
     */
    public void setLocal(String Local) {
        this.Local = Local;
    }

    /**
     * @return the Patrocinios
     */
    public String getPatrocinios() {
        return Patrocinios;
    }

    /**
     * @param Patrocinios the Patrocinios to set
     */
    public void setPatrocinios(String Patrocinios) {
        this.Patrocinios = Patrocinios;
    }

    /**
     * @return the Cartaz
     */
    public String getCartaz() {
        return Cartaz;
    }

    /**
     * @param Cartaz the Cartaz to set
     */
    public void setCartaz(String Cartaz) {
        this.Cartaz = Cartaz;
    }
    
}

