/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;




public class Palco {


    
    
    public enum tipoPalco {
        PRINCIPAL, SECUNDARIO
    }
    
    private int lotacao;
    private Equipamento equipamento;
    private tipoPalco tipoPalco;
    
    public Palco (){
        
    }
    
    public Palco (tipoPalco tipoPalco, int lotacao, Equipamento equipamento){
        setEquipamento(equipamento);
        setTipoPalco(tipoPalco);
        setLotacao(lotacao);
    }
    
        /**
     * @return the equipamento
     */
    public Equipamento getEquipamento() {
        return equipamento;
    }

    /**
     * @param equipamento the equipamento to set
     */
    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    /**
     * @return the tipoPalco
     */
    public tipoPalco getTipoPalco() {
        return tipoPalco;
    }

    /**
     * @param tipoPalco the tipoPalco to set
     */
    public void setTipoPalco(tipoPalco tipoPalco) {
        this.tipoPalco = tipoPalco;
    }
    
        /**
     * @return the lotacao
     */
    public int getLotacao() {
        return lotacao;
    }

    /**
     * @param lotacao the lotacao to set
     */
    public void setLotacao(int lotacao) {
        this.lotacao = lotacao;
    }
    

       @Override
    public String toString(){
        return String.format("\nPalco: %s \nLotacao: %d \nEquipamento: %s", getTipoPalco(), getLotacao(), getEquipamento().getCusto() + getEquipamento().getPac());
        
    }
   
    
}
