/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leng3_2db_grupo4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class ListaPalcos {
       private List<Palco> listaPalcos;    
    
    public ListaPalcos() {  
        listaPalcos = new ArrayList<>();
    }

    public boolean adicionarPalco(Palco palcos) {
        if(!listaPalcos.contains(palcos)) {
            return listaPalcos.add(palcos);
        }
        
        return false;
    }
    
    public boolean guardar(String nomeFicheiro) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(listaPalcos);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    } 
    
    public boolean ler(String nomeFicheiro) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(nomeFicheiro));
            try {
                listaPalcos=(List<Palco>)in.readObject();
            } finally {
                in.close();
            }
            return true;
        } catch (IOException|ClassNotFoundException ex) {
            return false;
        }
    }
    
    /**
     * @return the listaPalcos
     */
    public List<Palco> getListaPalcos() {
        return listaPalcos;
    }

    /**
     * @param listaPalcos the listaConvidados to set
     */
    public void setListaPalcos(List<Palco> listaPalcos) {
        this.listaPalcos = listaPalcos;
        
    }
    
}
